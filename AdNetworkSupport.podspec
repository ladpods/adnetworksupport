Pod::Spec.new do |s|

#   AdNetworkSupport description

    s.name      = "AdNetworkSupport"
    s.version   = "0.2.6"
    s.summary   = "AdNetworkSupport adapter"

    s.homepage  = "https://gitlab.com/ladpods/adnetworksupport"
    s.license   = 'MIT'
    s.author    = { "laddev" => "castro jean-baptiste <jean-baptiste.castro@lagardere-active.com>" }
    s.source    = { :git => "https://gitlab.com/ladpods/adnetworksupport.git", :tag => s.version.to_s }
    s.platform  = :ios, '8.0'


#   InMobi Adapter

    s.subspec 'InMobi' do |im|
        im.requires_arc         = true
        im.source_files         = 'Pod/InMobi/Classes/*'
        im.public_header_files  = 'Pod/InMobi/Classes/*.h'
        im.vendored_libraries   = 'Pod/InMobi/Libraries/libInMobi.a'
        im.library              = 'InMobi'
        im.xcconfig             = { 'LIBRARY_SEARCH_PATHS' => '$(PODS_ROOT)/AdNetworkSupport/Pod/InMobi/Libraries' }
        im.dependency             'mopub-ios-sdk', '~> 5.0'
        im.dependency             'sqlite3'
        im.frameworks           =  "AdSupport","AudioToolbox","AVFoundation","CoreGraphics","CoreMedia","CoreTelephony","EventKit","EventKitUI","MessageUI","StoreKit","SystemConfiguration"
    end



end
