#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "IMBanner.h"
#import "IMBannerDelegate.h"
#import "IMConstants.h"
#import "IMError.h"
#import "IMIncentivisedDelegate.h"
#import "IMInterstitial.h"
#import "IMInterstitialDelegate.h"
#import "IMNative.h"
#import "IMNativeDelegate.h"
#import "InMobi.h"
#import "InMobiAnalytics.h"
#import "InMobiBannerCustomEvent.h"
#import "InMobiInterstitialCustomEvent.h"
#import "InMobiNativeAdAdapter.h"
#import "InMobiNativeCustomEvent.h"

FOUNDATION_EXPORT double AdNetworkSupportVersionNumber;
FOUNDATION_EXPORT const unsigned char AdNetworkSupportVersionString[];

