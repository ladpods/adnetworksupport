//
//  main.m
//  AdNetworkSupport
//
//  Created by laddev on 01/20/2016.
//  Copyright (c) 2016 laddev. All rights reserved.
//

@import UIKit;
#import "LAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LAAppDelegate class]));
    }
}
