//
//  LAAppDelegate.h
//  AdNetworkSupport
//
//  Created by laddev on 01/20/2016.
//  Copyright (c) 2016 laddev. All rights reserved.
//

@import UIKit;

@interface LAAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
