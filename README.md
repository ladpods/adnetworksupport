# AdNetworkSupport

[![CI Status](http://img.shields.io/travis/laddev/AdNetworkSupport.svg?style=flat)](https://travis-ci.org/laddev/AdNetworkSupport)
[![Version](https://img.shields.io/cocoapods/v/AdNetworkSupport.svg?style=flat)](http://cocoapods.org/pods/AdNetworkSupport)
[![License](https://img.shields.io/cocoapods/l/AdNetworkSupport.svg?style=flat)](http://cocoapods.org/pods/AdNetworkSupport)
[![Platform](https://img.shields.io/cocoapods/p/AdNetworkSupport.svg?style=flat)](http://cocoapods.org/pods/AdNetworkSupport)

AdNetworkSupport is an ad adapter manager for MoPub SDK.

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

To use all adapters, add the line:

```ruby
pod "AdNetworkSupport"
```

To use an adapter separately:

####	Facebook Adapter
 ```ruby
pod "AdNetworkSupport/Facebook"
```

####	Flurry Adapter

 ```ruby
pod "AdNetworkSupport/Flurry"
```

####	Google Adapter

 ```ruby
pod "AdNetworkSupport/Google"
```

####	iAd Adapter

 ```ruby
pod "AdNetworkSupport/iAd"
```

####	InMobi Adapter

 ```ruby
pod "AdNetworkSupport/InMobi"
```

####	Millennial Adapter

 ```ruby
pod "AdNetworkSupport/Millennial"
```

####	MoPub Adapter

 ```ruby
pod "AdNetworkSupport/MoPub"
```

## Author

laddev, castro jean-baptiste <jean-baptiste.castro@lagardere-active.com>

## License

AdNetworkSupport is available under the MIT license. See the LICENSE file for more info.
